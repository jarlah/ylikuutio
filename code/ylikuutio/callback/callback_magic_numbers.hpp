// Ylikuutio - A 3D game and simulation engine.
//
// Copyright (C) 2015-2020 Antti Nuortimo.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __CALLBACK_MAGIC_NUMBERS_HPP_INCLUDED
#define __CALLBACK_MAGIC_NUMBERS_HPP_INCLUDED

#define ENTER_CONSOLE_MAGIC_NUMBER 0xc1e1ece  // "cielece" ~ "kieleke" in modern Finnish ortography, means "projecting part" in Finnish
#define EXIT_CONSOLE_MAGIC_NUMBER 0xaa110cc0  // "aallocco" ~ "aallokko" in modern Finnish ortography, means "swell of the sea/lake" in Finnish
#define CLEAR_CONSOLE_MAGIC_NUMBER 0xa1cac007 // "alcacoot" ~ "alkakoot" in modern Finnish ortography, means "let them begin" in Finnish
#define PAUSE_MAGIC_NUMBER 0x1c1a1ca          // "iciaica" ~ "ikiaika" in modern Finnish ortography, means "eternity" in Finnish
#define CONTINUE_MAGIC_NUMBER 0xede11e        // "edelle", means "ahead" or "before" in Finnish
#define EXIT_PROGRAM_MAGIC_NUMBER 0x0c0ea1ca  // "coeaica" ~ "koeaika" in modern Finnish ortography, means "trial period" or "probation" or "parole" in Finnish

#endif
